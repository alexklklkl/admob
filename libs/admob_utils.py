import os
import pickle
from google_auth_oauthlib.flow import Flow
from googleapiclient.discovery import build
from google.auth.transport.requests import Request


# Constants for the AdMob API service.
API_NAME = 'admob'
API_VERSION = 'v1'
API_SCOPE = 'https://www.googleapis.com/auth/admob.readonly'
CREDENTIALS_DIR = os.path.join(
  os.path.dirname(os.path.dirname(__file__)), 'credentials'
)
CREDENTIALS_FILE = os.path.join(
  CREDENTIALS_DIR,
  'client_secrets.json'
)
TOKEN_FILE = os.path.join(
  CREDENTIALS_DIR,
  'token.pickle'
)


# Authenticate user and create AdMob Service Object.
def authenticate():
  """Authenticates a user and creates an AdMob Service Object.

  Returns:
    An AdMob Service Object that is authenticated with the user using either
    a client_secrets file or previously stored access and refresh tokens.
  """

  # The TOKEN_FILE stores the user's access and refresh tokens, and is
  # created automatically when the authorization flow completes for the first
  # time.
  if os.path.exists(TOKEN_FILE):
    with open(TOKEN_FILE, 'rb') as token:
      credentials = pickle.load(token)

    if credentials and credentials.expired and credentials.refresh_token:
      credentials.refresh(Request())

  # If there are no valid stored credentials, authenticate using the
  # client_secrets file.
  else:
    flow = Flow.from_client_secrets_file(
        CREDENTIALS_FILE,
        scopes=[API_SCOPE],
        redirect_uri='urn:ietf:wg:oauth:2.0:oob')

    # Redirect the user to auth_url on your platform.
    auth_url, _ = flow.authorization_url()
    print('Please go to this URL: {}\n'.format(auth_url))

    # The user will get an authorization code. This code is used to get the
    # access token.
    code = input('Enter the authorization code: ')
    flow.fetch_token(code=code)
    credentials = flow.credentials

  # Save the credentials for the next run.
  with open(TOKEN_FILE, 'wb') as token:
    pickle.dump(credentials, token)

  # Build the AdMob service.
  admob = build(API_NAME, API_VERSION, credentials=credentials)
  return admob
