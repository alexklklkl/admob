import json
from libs import admob_utils
from libs.examples.get_account import get_account
from libs.examples.list_accounts import list_accounts
from libs.examples.generate_network_report import generate_network_report
from libs.examples.generate_mediation_report import generate_mediation_report

PUBLISHER_ID = "pub-3216124025478948"
INTENT_FOR_JSON = 2

report_examples = (
    get_account,
    list_accounts,
    generate_network_report,
    generate_mediation_report
)

service = admob_utils.authenticate()

for report in report_examples:
    result = report(service, PUBLISHER_ID)
    print(json.dumps(result, indent=INTENT_FOR_JSON))
