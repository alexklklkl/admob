# AdMob API Python

## Текущая версия API: 1.0
## EndPoints
- GET v1/accounts
- GET v1/accounts/pub-1234567890123456
- POST v1/accounts/pub-1234567890/mediationReport:generate
- POST v1/accounts/pub-1234567890/networkReport:generate

## Пример
- Установить requirements.txt:
   - google-auth-oauthlib
   - google-api-python-client
- Сформировать OAuth 2.0 Client ID с включенным AdMob API*
- Положить json с OAuth 2.0 Client ID в credentials/client_secrets.json
- Для получения отчетов выполнить main.py

**В примерах используется тип приложения "Desktop app", для сервера реализовать механизм и использовать тип "Web server"*

## Ресурсы
API Home: https://developers.google.com/admob/api
